#!/usr/bin/env python3
import fileinput
import re
import sys
import socket
from datetime import datetime
from easyzone import easyzone

ptrZoneName="" # Enter PTR zone name, example: 16.217.172.in-addr.arpa
ptrZoneFile="" # Enter full path to PTR zone file, example: /var/named/chroot/var/16.217.172.in-addr.arpa
ipRange="" # Enter ipRange: example: 172.217.16.

def process_line(line, ip, mode):
  if mode == "PTR":
    reg = re.compile('^%s..IN'%ip)
    lo = reg.match(line)
    if lo:
      host = line.rstrip().split('\t')
      return host
  elif mode == "Serial":
    reg = re.compile(';Serial')
    lo = reg.match(line)
    if lo:
      host = line.rstrip().split('\t')
      return host
     
def updatePtrZone(chngIP, chngHost, mode):
  updateSerial=False
  with fileinput.FileInput(ptrZoneName, inplace=True, backup='.bak') as file:
    for line in file:
      if mode == "PTR":
        host=process_line(line.rstrip(), chngIP, "PTR")
        if host:
          line = re.sub(host[3], chngHost + ".",line.rstrip())
          updateSerial=True
          print(line)
        else:
          print(line.rstrip())
      elif mode == "Serial":
          line = re.sub(str(chngIP), str(chngHost),line.rstrip())
          print(line)

  return updateSerial

def update_serial():
  z = easyzone.zone_from_file(ptrZoneName, ptrZoneFile)
  #z = easyzone.zone_from_file('179.119.88.in-addr.arpa', '179.119.88.in-addr.arpa')
  soa = z.root.soa
  oldSerial = soa.serial
  new_serial = int(datetime.now().strftime('%Y%m%d00',))
  print(soa.serial)
  if new_serial <= soa.serial:
      new_serial = soa.serial + 1
  soa.serial = new_serial

  updatePtrZone(oldSerial, soa.serial, "Serial")

def help():
  print("Usage: python3 reverse.py <IP> <my.domain.lt>")
  sys.exit(1)

def checkPTR(chngIP, chngHost):
  try:
    hostIP = socket.gethostbyname(chngHost)
    chngIP = ipRange + chngIP
    if hostIP == chngIP:
      return True
    else:
      return False
  
  except socket.gaierror:
    return False


def main():
  try:
    chngIP = sys.argv[1]
    chngHost = sys.argv[2]
    if checkPTR(chngIP, chngHost) == True:
      if updatePtrZone(chngIP, chngHost, "PTR") == True:
        update_serial()
    else:
      print(chngHost +" doesn't resolve to " + ipRange + chngIP)
  except IndexError:
    help()

main()